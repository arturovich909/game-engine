import keyboard as k
import turtle as t
import time

t.penup()

t.shape("turtle")

t.title("Turtle Paint")

t.clear()

tpos = 0
tdirection = 0

def player():
    t.circle(1)

while True:
    if k.is_pressed("down"):
        if k.is_pressed("space"):
            t.pendown()
        elif not k.is_pressed("space"):
            t.penup()
        tpos = -5
        t.forward(tpos)
        time.sleep(0.1)
    elif k.is_pressed("up"):
        if k.is_pressed("space"):
            t.pendown()
        elif not k.is_pressed("space"):
            t.penup()
        tpos = 5
        t.forward(tpos)
        time.sleep(0.1)
    elif k.is_pressed("right"):
        tdirection = 15
        t.right(tdirection)
        time.sleep(0.1)
    elif k.is_pressed("left"):
        tdirection = -15
        t.right(tdirection)
        time.sleep(0.1)
        
